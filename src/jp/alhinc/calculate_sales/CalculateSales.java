package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String Serial_Number = "売上ファイル名が連番になっていません";
	private static final String FILE_NOT_DIGITS = "合計金額が10桁超えました。";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMAP
		Map<String, String> commodityNames = new HashMap<>();
		//商品コード売上金
		Map<String, Long> commoditySales = new HashMap<>();
		//コマンドライン引数が渡されているのかの確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店定義ファイル")) {
			return;
		}

		//商品定義ファイルの読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[A-Za-z0-9]{8}$",
				"商品定義ファイル")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		BufferedReader br = null;
		//集計ファイルが入ってきている
		File[] files = new File(args[0]).listFiles();
		//file
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			files[i].getName();

			//ファイルなのかの確認
			//任意の８桁の数字＋.rcd
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}+.rcd$")) {
				rcdFiles.add(files[i]);

			}
		}

		//売上ファイルが連番なのかの確認
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(Serial_Number);
				return;
			}
		}

		//2-2
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;

				List<String> lines = new ArrayList<>();
				while ((line = br.readLine()) != null) {
					//0番目の要素に支店コード 1番目の要素に売上金額が格納されている【修正】
					lines.add(line);
					//System.out.println(lines.get(i));
				}

				//3行かの確認
				if (lines.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_FORMAT);
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルにあるかの確認
				if (!branchSales.containsKey(lines.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_FORMAT);
					return;
				}

				//売上金額が数字なのかの確認
				if (!lines.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//longに変換
				long fileSale = Long.parseLong(lines.get(2));
				//合計金額の加算
				Long saleAmount = branchSales.get(lines.get(0)) + fileSale;
				Long commoditySale = commoditySales.get(lines.get(1)) + fileSale;

				//売上金額が10桁超えるかの確認
				if (saleAmount >= 1000000000L) {
					System.out.println(FILE_NOT_DIGITS);
					return;
				}
				if(commoditySale>=1000000000L) {
					System.out.println(FILE_NOT_DIGITS);
					return;
				}

				//合計金額をMAPに保持
				branchSales.put(lines.get(0), saleAmount);
				commoditySales.put(lines.get(1), commoditySale);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
					}
				}
			}

			// 支店別集計ファイル書き込み処理
			if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
				return;
			}

			//商品定義の追加書き込み処理
			if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
				return;
			}
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Mapnames,
			Map<String, Long> Mapsales, String Match, String filenot) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//支店定義ファイル・商品定義ファイルが存在するか確認の条件式
			if (!file.exists()) {
				System.out.println(filenot + FILE_NOT_EXIST);
			}

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//支店定義ファイル・商品定義ファイルのフォーマット確認
				if ((items.length != 2) || (!items[0].matches(Match))) {
					System.out.println(filenot + FILE_INVALID_FORMAT);
					return false;
				}

				//Map(支店コード,支店名)を保持
				Mapnames.put(items[0], items[1]);
				//支店コードと売上金額を保持
				Mapsales.put(items[0], 0L);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Mapnames,
			Map<String, Long> Mapsales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for (String key : Mapsales.keySet()) {
				bw.write(key + "," + Mapnames.get(key) + "," + Mapsales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
